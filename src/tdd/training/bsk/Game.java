package tdd.training.bsk;

public class Game {

	private int index;
	private int firstBonusT;
	private int secondBonusT;
	private int maxFrame = 10;
	private int checkLast = maxFrame-1;
	private Frame[] game;
	
	
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		
		game = new Frame[maxFrame] ;
	
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		if(index < 10) {
			
			game[index] = frame; 
			
			index++;
			
			
		}
			
		
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {

		return game[index] ;	
		
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		
		firstBonusT = firstBonusThrow;
		
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {

		secondBonusT= secondBonusThrow;
		
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		
		return firstBonusT;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
	
		return secondBonusT;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		int totCalculateScore = 0;
		
		for(int j = 0;j<maxFrame;j++) {
			
			
			if(getFrameAt(j).isSpare()) {
				
				if(j == checkLast) {
					
					getFrameAt(j).setBonus(getFirstBonusThrow());
					
					totCalculateScore += getFrameAt(j).getScore() ;
					
				}else {
					
					getFrameAt(j).setBonus(getFrameAt(j+1).getFirstThrow());
				
					totCalculateScore += getFrameAt(j).getScore() ;
				
				}
			}else if (getFrameAt(j).isStrike() ){
				
				
				
				if( maxFrame>j+1  && getFrameAt(j+1).isStrike()  ) {
					
					if(j == checkLast-1) {
						
						getFrameAt(j).setBonus(getFrameAt(j+1).getFirstThrow()+ getFirstBonusThrow());
						
						totCalculateScore += getFrameAt(j).getScore() ;
						
						
					}else {
						
						getFrameAt(j).setBonus(getFrameAt(j+1).getFirstThrow()+getFrameAt(j+2).getFirstThrow());
						
						totCalculateScore += getFrameAt(j).getScore() ;
						
						
					}
					
					
					
				}else {
					 
					if(j == checkLast) {
						
						getFrameAt(j).setBonus(getFirstBonusThrow()+getSecondBonusThrow());
						
						totCalculateScore += getFrameAt(j).getScore() ;
						
						
					}else {
						
						getFrameAt(j).setBonus(getFrameAt(j+1).getFirstThrow()+getFrameAt(j+1).getSecondThrow());
						
						totCalculateScore += getFrameAt(j).getScore() ;
						
					}
			
					
				
				}
			}else{
			
				
				totCalculateScore += getFrameAt(j).getScore();
				
			}
			
			
			
		}
		
		
		
		return totCalculateScore;	
		
	}








}
