package tdd.training.bsk;

public class Frame {

	
	private int fistrThrowPinsknockedDown;
	private int secondThrowPinsknockedDown;
	private int bonusSpare;

	
	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		
		fistrThrowPinsknockedDown = firstThrow;
		secondThrowPinsknockedDown = secondThrow;
		
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		
		return fistrThrowPinsknockedDown;
		
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		
		return secondThrowPinsknockedDown;
		
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
	
		if(isSpare() || isStrike()) {
			
			bonusSpare = bonus;
		
		}
		
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		
		return bonusSpare;
		
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
	
	
		if(isSpare() || isStrike()) {
	
			return fistrThrowPinsknockedDown + secondThrowPinsknockedDown + getBonus()  ;
				
		}else {
			
			return fistrThrowPinsknockedDown + secondThrowPinsknockedDown ;
			
		}
		
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
	
		if(fistrThrowPinsknockedDown == 10 && secondThrowPinsknockedDown == 0 ) {
			
			return true;
			
		}else {
			
			return false;
		}
		
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		
		if((fistrThrowPinsknockedDown + secondThrowPinsknockedDown) == 10 && secondThrowPinsknockedDown !=0 ) {
			
			return true;
			
		}else {
	
			return false;
				
		}
	}

}
