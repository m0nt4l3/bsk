package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;



public class FrameTest {

	@Test
	public void frameReturnRight() throws Exception{
		
		int imputFristThrowFrame = 2;
		int imputsecondThrowFrame = 4;
		 
		Frame frame = new Frame(imputFristThrowFrame,imputsecondThrowFrame);
		
		assertEquals(2, frame.getFirstThrow());
		assertEquals(4, frame.getSecondThrow());
		
		
	}

	
	@Test
	public void scoreReturnRight() throws BowlingException{
		
		int imputFristThrowFrame = 2;
		int imputsecondThrowFrame = 6;
		 
		Frame frame = new Frame(imputFristThrowFrame,imputsecondThrowFrame);
		
		assertEquals(8, frame.getScore());
	
		
	}
	
	
	@Test
	public void scoreReturnRightWithSpare() throws BowlingException{
		
		int imputFristThrowFrame = 2;
		int imputsecondThrowFrame = 8;
		 
		Frame frame = new Frame(imputFristThrowFrame,imputsecondThrowFrame);
		
		frame.setBonus(2);
		
		assertEquals(12, frame.getScore());
		
		
	}
	
	
	
}
